﻿using System;

namespace BusinessLogic
{
    using FizzBuzz.Services.Implementations;
    using FizzBuzz.Services.Interface;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivisibleByFiveTests
    {
        private Mock<IDayProvider> dayProvider;

        [SetUp]
        public void Setup()
        {
            this.dayProvider = new Mock<IDayProvider>();
        }

        [TestCase(1, false)]
        [TestCase(5, true)]
        [TestCase(7, false)]
        public void IsValidTest(int value, bool expectedResult)
        {
            var result = new DivisibleByFiveRule(this.dayProvider.Object);
            var actualResult = result.IsValid(value);
            Assert.AreEqual(actualResult, expectedResult);
        }

        [TestCase(false, "buzz")]
        [TestCase(true, "wuzz")]
        public void TestForBuzzOrWuzz(bool day, string expectedResult)
        {
            var setUpValue = this.dayProvider.Setup(x => x.IsValid(DateTime.Now.DayOfWeek)).Returns(day);
            var result = new DivisibleByFiveRule(this.dayProvider.Object);
            var actualResult = result.DispalyFizzOrBuzz();
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
