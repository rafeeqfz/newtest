namespace FizzBuzzWeb.DependencyResolution
{
    using StructureMap.Graph;
    using StructureMap;
    using FizzBuzz.Services.Implementations;
    using FizzBuzz.Services.Interface;

    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });
                x.For<IFizzBuzzRule>().Use<DivisibleByThreeAndFiveRule>();
                x.For<IFizzBuzzRule>().Use<DivisibleByFiveRule>();              
                x.For<IFizzBuzzRule>().Use<DivisibleByThreeRule>();
                x.For<IFizzBuzzService>().Use<FizzBuzzService>();
                x.For<IDayProvider>().Use<DayProvider>();

              
            });
            return ObjectFactory.Container;
        }
    }
}