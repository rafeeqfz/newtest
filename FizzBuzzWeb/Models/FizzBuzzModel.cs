﻿namespace FizzBuzzWeb.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel;
    using PagedList;
 

    public class FizzBuzzModel
    {
        public IPagedList<string> FizzBuzzNumbers { get; set; }

        [RegularExpression(@"^\d+$")]
        [Required(ErrorMessage = "This filed is required")]
        [Range(1, 100, ErrorMessage = "Please enter the value between 1 to 1000")]
        [DisplayName("Enter Integer Number")]
        public int Input { get; set; }
    }
}