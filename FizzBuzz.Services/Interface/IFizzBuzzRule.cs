﻿namespace FizzBuzz.Services.Interface
{
    public interface IFizzBuzzRule
    {
        bool IsValid(int number);

        string DispalyFizzOrBuzz();
    }
}
