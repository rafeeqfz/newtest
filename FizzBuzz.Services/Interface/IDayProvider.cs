﻿namespace FizzBuzz.Services.Interface
{
    using System;

    public interface IDayProvider
    {
        bool IsValid(DayOfWeek dayOfWeek);
    }
}
