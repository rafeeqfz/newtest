﻿namespace FizzBuzz.Services.Interface
{
    using System.Collections.Generic;
 public   interface IFizzBuzzService
    {
        IEnumerable<string> GetFizzBuzzResult(int number);
    }
}
