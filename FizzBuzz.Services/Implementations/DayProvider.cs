﻿namespace FizzBuzz.Services.Implementations
{
    using FizzBuzz.Services.Interface;
    using System;

    public class DayProvider : IDayProvider
    {
        public bool IsValid(DayOfWeek dayOfWeek)
        {
            return dayOfWeek == DayOfWeek.Monday;
        }
    }
}
