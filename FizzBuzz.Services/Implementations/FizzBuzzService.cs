﻿namespace FizzBuzz.Services.Implementations
{
    using FizzBuzz.Services.Interface;
    using System.Collections.Generic;
    using System.Linq;

    public class FizzBuzzService : IFizzBuzzService
    {
        private IEnumerable<IFizzBuzzRule> fizzBuzzRule;

        public FizzBuzzService(IList<IFizzBuzzRule> fizzBuzzRule)
        {
            this.fizzBuzzRule = fizzBuzzRule;
        }

        public IEnumerable<string> GetFizzBuzzResult(int number)
        {
            List<string> fizzBuzzList = new List<string>();
            for (int i = 1; i <= number; i++)
            {
                string output = i.ToString();

                var result = this.fizzBuzzRule.FirstOrDefault(a => a.IsValid(i));

                if (result != null)
                {
                    output = result.DispalyFizzOrBuzz();
                }
                fizzBuzzList.Add(output);
            }
            return fizzBuzzList;
        }
    }
}