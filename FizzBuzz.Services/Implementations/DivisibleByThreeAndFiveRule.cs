﻿namespace FizzBuzz.Services.Implementations
{
    using FizzBuzz.Services.Interface;
    using System;

    public class DivisibleByThreeAndFiveRule : IFizzBuzzRule
    {
        private IDayProvider dayProvider;

        public DivisibleByThreeAndFiveRule(IDayProvider dayProvider)
        {
            this.dayProvider = dayProvider;
        }

        public bool IsValid(int number)
        {
            return ((number % 3 == 0) && (number % 5 == 0));
        }

        public string DispalyFizzOrBuzz()
        {
            return this.dayProvider.IsValid(DateTime.Now.DayOfWeek) ? "buzz wuzz" : "fizz buzz";
        }
    }
}
