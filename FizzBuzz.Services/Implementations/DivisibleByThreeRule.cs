﻿namespace FizzBuzz.Services.Implementations
{
    using FizzBuzz.Services.Interface;
    using System;

    public class DivisibleByThreeRule : IFizzBuzzRule
    {
        private IDayProvider dayProvider;

        public DivisibleByThreeRule(IDayProvider dayProvider)
        {
            this.dayProvider = dayProvider;
        }

        public bool IsValid(int number)
        {
            return number % 3 == 0;
        }

        public string DispalyFizzOrBuzz()
        {
            return this.dayProvider.IsValid(DateTime.Now.DayOfWeek) ? "wizz" : "fizz";
        }

    }
}
